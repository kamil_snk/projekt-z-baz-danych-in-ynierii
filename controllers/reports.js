/**
 * Created by ania on 2015-05-22.
 */
var express = require('express');
var session = require('../models/session');
var user = require('../models/user');
var paper = require('../models/paper');
var accommodation = require('../models/hotel');
var review = require('../models/review');
var reviewers = require('../models/user');
var calendar = require('../models/conference');
var jade = require('jade');
var fs = require('fs');
var pdf = require('html-pdf');

var options = { format: 'Letter', border: 50 };
 
module.exports = function (passport) {
    var router = express.Router();
    
    router.get('/report_accommodation', function(req, res, next) {
        accommodation.SelectAllAccommodation(function (zakw) {
            res.render('report/report_accommodation', {zakwaterowanie:zakw});
        });
    });
    
    router.get('/report_accommodation/dld', function(req, res, next) {
        accommodation.SelectAllAccommodation(function (zakw) {
            var fn = jade.compileFile('./views/report/report_accommodation_pdf.jade');
            var html = fn({zakwaterowanie:zakw});
            pdf.create(html, options).toStream(function(err, stream){
              stream.pipe(res);
            });
        });
        
    });
    
    router.get('/report_articles', function(req, res, next) {
        paper.SelectAllArticles (function (art) {
            res.render('report/report_articles', {artykuly:art});
        });
    });
    
    router.get('/report_articles/dld', function(req, res, next) {
    paper.SelectAllArticles(function (art) {
        var fn = jade.compileFile('./views/report/report_articles_pdf.jade');
        var html = fn({artykuly:art});
        pdf.create(html, options).toStream(function(err, stream){
             stream.pipe(res);
        });
      });
    });

        
    router.get('/report_review', function(req, res, next) {
        review.SelectAllReviews(function (rec) {
        res.render('report/report_review', {recenzje:rec});
        });
    });
    
    router.get('/report_review/dld', function(req, res, next) {
    review.SelectAllReviews(function (rec) {
            var fn = jade.compileFile('./views/report/report_review_pdf.jade');
            var html = fn({recenzje:rec});
            pdf.create(html, options).toStream(function(err, stream){
              stream.pipe(res);
            });
        });
        
    });
    router.get('/report_reviewers', function(req, res, next) {
        reviewers.SelectAllLoad(function (rev) {
        res.render('report/report_reviewers', {recenzenci:rev});
        });
    });
    
    router.get('/report_reviewers/dld', function(req, res, next) {
        reviewers.SelectAllLoad(function (rev) {
            var fn = jade.compileFile('./views/report/report_reviewers_pdf.jade');
            var html = fn({recenzenci:rev});
            pdf.create(html, options).toStream(function(err, stream){
              stream.pipe(res);
            });
        });
        
    });    
    router.get('/report_user', function(req, res, next) {
        user.SelectAllParticipants (function (tmp1) {
            user.SelectAllAdmins (function (tmp2) {
                user.SelectAllReviewers (function (tmp3) {
                    res.render('report/report_user',{uczestnicy:tmp1, administratorzy:tmp2, recenzenci:tmp3});
                });   
            });
        });
    });
    
    router.get('/report_user/dld', function(req, res, next) {
        user.SelectAllParticipants (function (tmp1) {
            user.SelectAllAdmins (function (tmp2) {
                user.SelectAllReviewers (function (tmp3) {
                var fn = jade.compileFile('./views/report/report_user_pdf.jade');
                var html = fn({uczestnicy:tmp1, administratorzy:tmp2, recenzenci:tmp3});
                    pdf.create(html, options).toStream(function(err, stream){
                    stream.pipe(res);
                    });
                });   
            });
        });
    });
    
    router.get('/calendar', function(req, res, next) {
        calendar.CreateCalendar(function (kal) {
        res.render('report/calendar', {kalendarz:kal});
        });
    });
    
    router.get('/calendar/dld', function(req, res, next) {
        calendar.CreateCalendar(function (kal) {
            var fn = jade.compileFile('./views/report/calendar_pdf.jade');
            var html = fn({kalendarz:kal});
            pdf.create(html, options).toStream(function(err, stream){
              stream.pipe(res);
            });
        });
        
    });
    return router;
};