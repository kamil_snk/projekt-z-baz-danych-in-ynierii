/**
 * Created by Bartek on 2015-05-12.
 */
var express = require('express');
var conference = require('../models/conference');
var user = require('../models/user');
var participant = require('../models/participant');
var session = require('../models/session');
var hotel = require('../models/hotel');
var charges = require('../models/charges');
var checkUtil = require('../common/checkutil');
var auth = require('../config/auth');

var items = [
    {code: 'A1', name: 'Soda1', description: 'desc1'},
    {code: 'A2', name: 'Soda2', description: 'desc2'},
    {code: 'A3', name: 'Soda3', description: 'desc3'},
    {code: 'A4', name: 'Soda4', description: 'desc4'},
    {code: 'A5', name: 'Soda5', description: 'desc5'},
    {code: 'A6', name: 'Soda6', description: 'desc6'},
    {code: 'A7', name: 'Soda7', description: 'desc7'},
    {code: 'A8', name: 'Soda8', description: 'desc8'},
    {code: 'A9', name: 'Soda9', description: 'desc9'},
    {code: 'A10', name: 'Soda10', description: 'desc10'},
    {code: 'A11', name: 'Soda11', description: 'desc11'}
];

module.exports = function (passport) {
    var router = express.Router();
    

    router.get('/conference/registration_for_participation',auth.isUser, function(req, res, next) {
        conference.getWhereNotParticipating(req.user.id_uzytkownika, function(all_conference){
        hotel.selectAll(function (all_hotel) {
            res.render('conference/registration_for_participation', {konferencja:all_conference,hotel:all_hotel});
            });
        });

    });
    
    router.post('/conference/registration_for_participation',auth.isUser, function (req, res,next) {
        var confp = {
                    konferencja_id_konferencja: parseInt(req.body.konferencja),
                    hotel_id_hotel: req.body.hotel,
                    uzytkownik_id_uzytkownika: req.user.id_uzytkownika
                };
        participant.registerForUser(confp, function (){
        var ucz = {
            uczestnik: 1
            };
            user.updateRowByField({id_uzytkownika: req.user.id_uzytkownika}, ucz, function(){
            res.redirect('/conference/conference_participants_list');
            });
        });
    });

    

    router.get('/conference/conference_add',auth.isAdmin, function(req, res,next) {
        
            session.selectAll(function(all_sessions){
                console.log(all_sessions);
                res.render('conference/conference_add', {session:all_sessions });
            
        });
    });
    
     router.get('/conference/conference_edit',auth.isAdmin, function(req, res, next) {
        res.render('conference/conference_edit', {items: items });
            
    
    });
       
    router.get('/conference/conference_add_success',auth.isAdmin, function(req, res, next) {
        res.render('conference/conference_add_success', {items: items });
            
    
    });
       
        router.get('/conference/conference',auth.isAdmin, function(req, res, next) {
        res.render('conference/conference', {items: items });

    });


     router.get('/conference_edit/:id',auth.isAdmin, function (req, res) {
        conference.selectRowByField({id_konferencja: req.params.id}, function (err, conference) {
            console.log(conference);
                res.render('conference/conference_edit', {edited_conference: conference});
        });
    });

    router.post('/conference_add',auth.isAdmin, function(req, res, next) {
        
     var nowy={
         
         nazwa : req.body.nazwa,
         adres : req.body.adres,
         data_rozpoczecia : req.body.datroz,
         data_zakonczenia : req.body.datzak,
         aktywna : req.body.optionAktywna,
         koszty :  req.body.koszty,
         sesja_id_sesja : req.body.sesja,
         czas : req.body.czas
     
       };
       
       console.log(nowy);
       
       conference.create(nowy, function (id){
           koszt={
            kwota: req.body.koszty,
            nazwa: req.body.nazwa,
            konferencja_id_konferencja: id,    
           }
        charges.createCost(koszt,function(){
        
           
           
         res.redirect('/conference/conference_add_success')  
       });
        
       });
    });
       
    router.post('/conference_edit/:id',auth.isAdmin, function(req, res, next) {
     
     var nowy={
         
         nazwa : req.body.nazwa,
         adres : req.body.adres,
         data_rozpoczecia : req.body.datroz,
         data_zakonczenia : req.body.datzak,
         aktywna : req.body.optionAktywna === '1',
         koszty :  req.body.koszty,
         czas : req.body.czas
     
       };
       
       console.log(nowy);
       
      conference.updateRowByField({id_konferencja: req.params.id}, nowy, function () {
            res.redirect('/conference/conference_info')
       });
    });
       
    router.get('/conference/conference_info',auth.isAdmin, function(req, res, next) {
            conference.ShowConferences(function (conf1) {
            res.render('conference/conference_info', { conference: conf1 });
            });
    });
     
    
  
    router.get('/conference/registration_selected/:id',auth.isUser, function(req, res, next) {
         conference.selectRowByField({id_konferencja: req.params.id}, function (err, conference) {
            console.log(conference);
            res.render('conference/registration_selected', {selected_conference: conference});
        });
            
    
    });
    
    router.post('/conference/registration_selected/:id',auth.isUser, function (req, res) {
        
        var uczestnik={

            zakwaterowanie: false,
            referat: false,
            //hotel_id_hotel: 0,
            uzytkownik_id_uzytkownika: req.user.id_uzytkownika
            
        };
        if(req.body.optionAktywna === '1')
        { 
         uczestnik.zakwaterowanie = true;
         uczestnik.hotel_id_hotel = 0;  
        }
        
        participant.registerForUser(uczestnik, function (id_uczest){
            var nowy={
                uczestnik_id_uczestnik: id_uczest,
                konferencja_id_konferencja: req.params.id
            };
            conference.AddIntoConference(nowy, function (id){
                res.redirect('/conference/conference_participants_list');
            });
        });
    });
    router.get('/conference/conference_participants_list',auth.isUser, function(req, res, next) {
            conference.ShowConferences(function (conf1) {
            res.render('conference/conference_participants_list', { conference: conf1 });
            });
        });
    router.get('/conference_participants/:id',auth.isUser, function (req, res,next) {
        conference.ShowParticipants({id_konferencja: req.params.id}, function (users) {
            console.log(users);
            res.render('conference/conference_participants', {uzytkownik: users});
        });
    });
    
    router.get('/conference/session_add',auth.isAdmin, function(req, res, next) {
        res.render('conference/session_add', {items: items });
    });
    
    router.post('/session_add',auth.isAdmin, function(req, res, next) {
        var sesja={
            nazwa:req.body.nazwa
        }
        session.create(sesja, function (id){
            console.log(sesja);
            res.render('conference/session_add', {message: "Dodano sesje o id: "+ id});
        });
    });
    
    router.get('/session_edit/:id',auth.isAdmin, function (req, res) {
        session.selectRowByField({id_sesja: req.params.id}, function (err, sesja) {
            console.log(sesja);
                res.render('conference/session_edit', {edited_session: sesja});
        });
    });
    
     router.post('/session_edit/:id',auth.isAdmin, function(req, res, next) {
        var sesja={
            nazwa:req.body.nazwa
        }
        session.updateRowByField({id_sesja: req.params.id}, sesja, function () {
            res.redirect('/conference/session_list');
        });
    });
    
    router.get('/conference/session_list',auth.isAdmin, function (req, res) {
        session.getSessions(function (sesja) {
            res.render('conference/session_list',{sesja:sesja});
        });
    });
    
    return router;
}
